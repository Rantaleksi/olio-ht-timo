// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Antti
 */
public class FXMLDocumentController implements Initializable {
    
    
    private Label label;
    @FXML
    private ComboBox<String> spCombo;
    @FXML
    private WebView webViewArea;
    @FXML
    private ComboBox<Item> itemBox;
    @FXML
    private RadioButton firstClassButton;
    @FXML
    private RadioButton secondClassButton;
    @FXML
    private RadioButton thirdClassButton;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private ComboBox<String> departureBox;
    @FXML
    private ComboBox<Smartpost> departureSmartPost;
    @FXML
    private ComboBox<String> destinationBox;
    @FXML
    private ComboBox<Smartpost> destinationSmartPost;
    
    // initialize the classes outside methods to make sure they can be used anywhere in the controller
    private Smartposts smartposts = Smartposts.getInstance();
    private Storage storage = Storage.getInstance(); // Tämä ei ole missään käytössä vielä
    private Items items = Items.getInstance();
    private File outputFile;
    private String memory;
    private double totalDistance;
    
    @FXML
    private ToggleGroup packageClass;
    @FXML
    private ComboBox<Package> packageBox;
    @FXML
    private ListView<String> listLog;
    @FXML
    private Label errorArea;
    @FXML
    private Button okButton;
    @FXML
    private Label errorField;
    @FXML
    private ListView<Package> changeListView;
    @FXML
    private Button confirmButton;
    @FXML
    private Label infoLabel;
    @FXML
    private ImageView imageView;
    @FXML
    private Label pictureLabel;
    @FXML
    private ImageView logoView;
    private Label eArea;
    @FXML
    private Button mapButton;
    @FXML
    private Button removeButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button refreshMap;
    @FXML
    private Label packageTxtLabel;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button addItemButton;
    @FXML
    private Button changePackage;
    @FXML
    private Button classInfoButton;
    @FXML
    private Button emptyLogButton;
    @FXML
    private Button saveLogButton;
    @FXML
    private Button downloadItemBt;
    @FXML
    private TextField listInputField;
    @FXML
    private CheckBox fastShipmentCheck;
    @FXML
    private Label selectedName;
    @FXML
    private Label selectedWeight;
    @FXML
    private Label selectedSize;
    @FXML
    private Button addFeatherBt;
    
    private int addedWeight = 0;
    
    private String newDate;
    
    private int totalPackages = 0;
    private double allTimeDistance = 0;
    
    @FXML
    private DatePicker calendar;
    @FXML
    private TextArea historyArea;
    @FXML
    private Button storageButton;
    @FXML
    private Button saveStorageButton;
    @FXML
    private ListView<String> historyView;
    
//    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

            webViewArea.getEngine().load(getClass().getResource("index.html").toExternalForm()); // launch the webview  for map
            spCombo.getItems().addAll(smartposts.getCities()); // add cities to the combobox in the first tab
            
            // add basic items to the item combobox
            itemBox.getItems().addAll(items.getItemList());
            
            // add cities to departure and destination comboboxes
            departureBox.getItems().addAll(smartposts.getCities());
            destinationBox.getItems().addAll(smartposts.getCities());
            
            // add smartposts to departure and destination comboboxes
            departureSmartPost.getItems().addAll(smartposts.getSmartPosts());
            destinationSmartPost.getItems().addAll(smartposts.getSmartPosts());
            
            
            firstClassButton.setUserData(1);
            secondClassButton.setUserData(2);
            thirdClassButton.setUserData(3);
            
            okButton.setVisible(false);
            changeListView.setVisible(false);
            infoLabel.setVisible(false);
            confirmButton.setVisible(false);
            pictureLabel.setVisible(false);
            addFeatherBt.setVisible(false);
            
            Image logo = new Image(getClass().getResourceAsStream("/Pictures/timoLogo.png"));
            logoView.setImage(logo);
            logoView.setPreserveRatio(true);
            logoView.setSmooth(true);
            logoView.setCache(true);
            
            
            String historyFile = "usagehistory.txt"; 
            try (BufferedReader bufr = new BufferedReader(new FileReader(historyFile))) {
                String[] tempPackages = bufr.readLine().split(":");
                String[] tempDistance = bufr.readLine().split(":");

                totalPackages = Integer.parseInt(tempPackages[1]);
                allTimeDistance = Double.parseDouble(tempDistance[1]);
                bufr.close();
            } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
        } // end of method // end of method

           
                     
    

    @FXML
    private void addSmartPosts(ActionEvent event) { // add smartposts on to map

        if (spCombo.getValue() == null) {
            errorArea.setVisible(true);
            errorArea.setText("Virhe! Sinun pitää valita kaupunki\n"
                    + " ennen kuin voit lisätä kyseisen\n"
                    + " kaupungin Smartpostit kartalle\n"
                    + " Paina OK jatkaaksesi!");
            okButton.setVisible(true);
           
        } else {
        
        String city = spCombo.getValue();
        listLog.getItems().add("Lisättiin kaupungin " + city + "Smartpostit kartalle");

        for (int i = 0; i < smartposts.getSmartPosts().size(); i++) {
            if (smartposts.getSmartPosts().get(i).getCity().equals(city)) {
                Smartpost sp = smartposts.getSmartPosts().get(i);
                String address = sp.getAddress() + "," + sp.getCode() + " " + sp.getCity();
                String info = sp.getPostOffice() + " " + sp.getAvailability();

                webViewArea.getEngine().executeScript("document.goToLocation('"+ address +"', '" + info + "', 'red')"); 
            }
        }
    }
    } // end of method

    @FXML
    private void removeSmartposts(ActionEvent event) {
        
        listLog.getItems().add("Poistettiin pakettien reitit kartalta");
        webViewArea.getEngine().executeScript("document.deletePaths()"); // delete the smartposts from the map
    }

    @FXML
    private void filterDepList(MouseEvent event) { // filters departure smartpostlist if city is set
        
        if (departureBox.getValue() != null) {
            departureSmartPost.getItems().clear();
            departureSmartPost.getItems().addAll(smartposts.getSmartPosts(departureBox.getValue()));
        
    }
    }

    @FXML
    private void filterDestList(MouseEvent event) { // filters destination smartpostlist if city is set
        
        if (destinationBox.getValue() != null) {
            destinationSmartPost.getItems().clear();
            destinationSmartPost.getItems().addAll(smartposts.getSmartPosts(destinationBox.getValue()));
        }
    }

    @FXML
    private void addToItemList(ActionEvent event) { // adds user-made item to itemlist

        if (nameField.getText().isEmpty()) {
            errorField.setText("Syötä esineelle nimi!");
        } else if (sizeField.getText().isEmpty()) {
            errorField.setText("Syötä esineen koko!");
        } else if (weightField.getText().isEmpty()) {
            errorField.setText("Syötä esineen paino!");
        } else {
        // check if the item already exists in the list !
        // NOT WORKING YET !!
            for (int j = 0; j < items.getItemList().size(); j++) {
                if (nameField.getText().equals(items.getItemList().get(j).getItemName())) {
                errorField.setText("Kyseinen esine on jo listassa, pönttö!");
                break;
            }
        
        }
            
            String[] parts = sizeField.getText().split("\\*");

            
            if (parts.length != 3) {
                errorField.setText("Virheellinen syöte!");
                return;
            } else {    
            
                try {
                    
                
                String width = parts[0];
                String depth = parts[1];
                String heigth = parts[2];
                items.addItem(new Item(nameField.getText(), Double.parseDouble(weightField.getText()), Integer.parseInt(width), Integer.parseInt(depth),
                       Integer.parseInt(heigth), fragileCheck.isSelected()));
                errorField.setText(null);
                itemBox.getItems().clear(); // refresh the item list
                itemBox.getItems().addAll(items.getItemList());

                String logOutput;
                logOutput = "Lisättiin esinelistaan: \n"
                        + "Nimi " + nameField.getText() + "\n"
                        + "Koko " + sizeField.getText() + " kuutiosenttimetriä" + "\n"
                        + "Paino " + weightField.getText() + " kilogrammaa" + "\n";

                if (fragileCheck.isSelected()) {
                logOutput = logOutput + "Särkyvä esine";  
                } else {
                logOutput = logOutput + "Ei särkyvä esine";
                }
                listLog.getItems().add(logOutput);

                nameField.clear();
                sizeField.clear();
                weightField.clear();
                fragileCheck.setSelected(false);
            } catch (NumberFormatException ex) {
                errorField.setText("Esineen luominen epäonnistui!");
            }
        }
    }
} // end of method


    @FXML
    private void createPackageAction(ActionEvent event) throws IOException {
        
        if (itemBox.getValue() == null && packageClass.getToggles().isEmpty()
                && departureSmartPost.getValue() == null && destinationSmartPost.getValue() == null) {
            errorField.setText("Et ole valinnut tarvittavia tietoja paketin luomiseksi!");
        } else { 
        
        // check whether the user has selected an item or package class at all or whether the item selected is too heave for 1. class package
        if (itemBox.getValue() == null) {
            errorField.setText("Valitse ensin esine!");
            return;
        } else if (!firstClassButton.isSelected() && !secondClassButton.isSelected() && !thirdClassButton.isSelected()) {
            errorField.setText("Valitse pakettiluokka!");
            return;
        } else if (((int)itemBox.getValue().getSizeList().get(0) > 25 || (int)itemBox.getValue().getSizeList().get(1) > 25 || (int)itemBox.getValue().getSizeList().get(2) > 25) 
                && packageClass.getSelectedToggle().getUserData().equals(2)) {
            errorField.setText("Liian suuri esine 2. luokan pakettiin!");
            return;
        } else if (itemBox.getValue().isFragile() && packageClass.getSelectedToggle().getUserData().equals(1)) {
            errorField.setText("Tämä esine tulee hajoamaan TIMO-miehen käsittelyssä,\n"+
                    "jos valitset ensimmäisen luokan paketin.");
            return;
        } else if ((itemBox.getValue().getWeight() + addedWeight) < 15 && itemBox.getValue().isFragile() && packageClass.getSelectedToggle().getUserData().equals(3)) {

            errorField.setText("TIMO-mies purkaa stressiään 3. luokan paketteihin.\n"+
                "Suosittelemme lisäämään höyheniä pakettiin!\n"
                + "Lisää höyheniä painamalla alle ilmestynyttä nappulaa!");
            addFeatherBt.setVisible(true);
            return;
            
        } else if (itemBox.getValue().getWeight() > 5 && packageClass.getSelectedToggle().getUserData().equals(1)) {
            errorField.setText("Liian painava esine 1. luokan paketiksi! \n"
            + "Valitse toinen pakettiluokka");
            packageClass.getSelectedToggle().selectedProperty().setValue(false);
            return;
        } else if (((int)itemBox.getValue().getSizeList().get(0) > 30 ||
                    (int)itemBox.getValue().getSizeList().get(1) > 60 ||
                    (int)itemBox.getValue().getSizeList().get(2) > 30)
                    && packageClass.getSelectedToggle().getUserData().equals(1)) {
                        errorField.setText("Liian suuri esine 1. luokan paketiksi");
                        return;
        } else if (((int)itemBox.getValue().getSizeList().get(0) > 20 ||
                    (int)itemBox.getValue().getSizeList().get(1) > 60 ||
                    (int)itemBox.getValue().getSizeList().get(2) > 20)
                    && packageClass.getSelectedToggle().getUserData().equals(2)) {
                        errorField.setText("Liian suuri esine 2. luokan paketiksi");
                        return;
        } else if (((int)itemBox.getValue().getSizeList().get(0) > 100 ||
                    (int)itemBox.getValue().getSizeList().get(1) > 60 ||
                    (int)itemBox.getValue().getSizeList().get(2) > 50)
                    && packageClass.getSelectedToggle().getUserData().equals(3)) {
                        errorField.setText("Liian suuri esine 3. luokan paketiksi");
                        return;
        
        }
        
        
        if ((departureSmartPost.getValue() == null || destinationSmartPost.getValue() == null)) {
            errorField.setText("Error! Et ole valinnut paketin lähetystietoja!");

        } else {
         
            Package p = new Package((int)packageClass.getSelectedToggle().getUserData(),
            itemBox.getValue(),
            departureSmartPost.getValue().getGeopoint().getGeoArray(),
            destinationSmartPost.getValue().getGeopoint().getGeoArray(),
            itemBox.getValue().isFragile(),
            departureSmartPost.getValue().getAddress(),
            destinationSmartPost.getValue().getAddress(),
            departureSmartPost.getValue().getAvailability(),
            departureSmartPost.getValue().getPostOffice(),
            departureSmartPost.getValue().getCity(),
            departureSmartPost.getValue().getCode(),
            destinationSmartPost.getValue().getAvailability(),
            destinationSmartPost.getValue().getPostOffice(),
            destinationSmartPost.getValue().getCity(),
            destinationSmartPost.getValue().getCode(),
            departureSmartPost.getValue(),
            destinationSmartPost.getValue(),
            itemBox.getValue().getWeight()+addedWeight);


            storage.addPackage(p);
            packageBox.getItems().clear(); // clear the package list
            packageBox.getItems().addAll(storage.getStorage());
            addedWeight = 0;
            String logOutput;
            logOutput = "Luotiin uusi paketti! " + "\n" + "Sisältö: " +p.getItem().getItemName() +"\n" +
                "Mistä: " +p.getDepartureAddress() +"\n" +
                "Minne: " + p.getDestinationAddress() +"\n" +
                "Pakettiluokka: " + p.getPackageClass() +"\n";

            if (p.isFragile()) {
                logOutput = logOutput + "Särkyvä esine";  
            } else {
                logOutput = logOutput + "Ei särkyvä esine";
            }
            listLog.getItems().add(logOutput);
            listLog.getItems().add("Paketteja yhteensä: " + Integer.toString(storage.getPackageCount()));
                
            // clears the package lists and departure & destination boxes for both cities and smartposts
            itemBox.setValue(null);
            packageClass.getSelectedToggle().selectedProperty().setValue(false);
            fastShipmentCheck.selectedProperty().setValue(false);
            departureBox.setValue(null);
            destinationBox.setValue(null);
            departureSmartPost.setValue(null);
            destinationSmartPost.setValue(null);
            firstClassButton.setVisible(true);
            secondClassButton.setVisible(true);
            thirdClassButton.setVisible(true);
            
            selectedName.setText("");
            selectedWeight.setText("");
            selectedSize.setText("");
            imageView.setVisible(false);

            errorField.setText(null);
        }
    }
}
        
       // end of method

    @FXML
    private void sendPackageAction(ActionEvent event) {

        if (packageBox.getValue() == null) {
            errorArea.setText("Error! Et ole valinnut pakettia lähetettäväksi!" + "\n"
            + "Paina OK jatkaaksesi.");
            errorArea.setVisible(true);
            okButton.setVisible(true);
        } else {
            ArrayList<Double> geoArray = new ArrayList<>();
            geoArray = packageBox.getValue().getGeoArray();
        
            int pClass = packageBox.getValue().getPackageClass();
            String colour = "";
            
            if (pClass == 1) {
                colour = "blue";
            } else if (pClass == 2) {
                colour = "green";
            } else if (pClass == 3) {
                colour = "red";
            }
//            
            String startAddress = packageBox.getValue().getDepartureAddress() + "," + packageBox.getValue().getDepPostcode() + " " +
                    packageBox.getValue().getDepCity();
            String endAddress = packageBox.getValue().getDestinationAddress() + "," + packageBox.getValue().getDestPostcode() + " " +
                    packageBox.getValue().getDestCity();
            String startInfo = packageBox.getValue().getDepPostOffice() + " " + packageBox.getValue().getDepAvailability();
            String endInfo = packageBox.getValue().getDestPostOffice() + " " + packageBox.getValue().getDestAvailability();
//
               webViewArea.getEngine().executeScript("document.goToLocation('"+ startAddress +"', '" + startInfo + "', 'blue')");
               webViewArea.getEngine().executeScript("document.goToLocation('"+ endAddress +"', '" + endInfo + "', 'green')");
//            }


            double distance = (double) webViewArea.getEngine().executeScript("document.createPath("+ geoArray +",'" + colour + "','" + pClass +"')");
            if (pClass == 1 && packageBox.getValue().isFragile() == true) {
                errorArea.setText("Älä lähetä särkyvää tavaraa 1. luokassa" + "\n"
                        + "Käy vaihtamassa pakettiluokkaa"
                        + "Paina OK jatkaaksesi.");
                errorArea.setVisible(true);
                okButton.setVisible(true);
                webViewArea.getEngine().executeScript("document.deletePaths()");
            } else if (distance > 150 && pClass == 1) {
                errorArea.setText("Ensimmäisen luokan pakettia ei voi lähettää\n"
                        + " yli 150km päähän" + "\n" 
                + "Paina OK jatkaaksesi.");
                errorArea.setVisible(true);
                okButton.setVisible(true);
                webViewArea.getEngine().executeScript("document.deletePaths()");
            } else if (pClass == 3 && packageBox.getValue().isFragile() == true && packageBox.getValue().getItem().getWeight() < 15){
                errorArea.setText("Kolmannen luokan paketti saattaa olla liian kevyt"
                        + " ja saattaa hajota jos TIMO-ukolla on jostain huono päivä"+ "\n"
                + "Paina OK jatkaaksesi.");
                errorArea.setVisible(true);
                okButton.setVisible(true);
                webViewArea.getEngine().executeScript("document.deletePaths()");
            } else 
                totalDistance = (totalDistance + distance);
                listLog.getItems().add("Paketti lähetetty kohteeseen " + packageBox.getValue().getDestinationAddress() +
                        ", " +packageBox.getValue().getDestCity() +
                        "\nPaketin matkan pituus: " + distance + " kilometriä\n" +
                "Paketit matkanneet yhteensä: " + totalDistance + " kilometriä.");
                packageBox.setValue(null); // Clears the package box after sending the package
                
                totalPackages++;
                allTimeDistance += totalDistance;
                
                String historyFile = "usagehistory.txt"; 
                    try (BufferedWriter hiswrite = new BufferedWriter(new FileWriter(historyFile))) {
                        hiswrite.write("Paketteja lähetetty yhteensä:"+totalPackages+"\n");
                        hiswrite.write("Pakettien kulkemat matkat yhteensä:"+ allTimeDistance+"\n");
                        hiswrite.close();
            } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
                
                storage.removePackage(packageBox.getValue()); // delete the item from the storage
                packageBox.getItems().clear(); // clears the package combobox
                packageBox.getItems().addAll(storage.getStorage());
        }
    
              
} // end of class

    @FXML
    private void okAction(ActionEvent event) {

        // ErrorArea is under the map
        // Error triggers cause a label describing the error and a button to close the message to be set to visible.
        // Clicking the OK button hides both the text area and button.

        errorArea.setText("");
        errorArea.setVisible(false);
        okButton.setVisible(false);
    }

    @FXML
    private void refreshMapAction(ActionEvent event) {
        //refreshes the map when user presses the button
        listLog.getItems().add("Päivitettiin kartta");
        webViewArea.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
    }

    @FXML
    private void emptyLog(ActionEvent event) {
        
        // empties the log
        listLog.getItems().clear();
        
    }

    @FXML
    private void saveLog(ActionEvent event) throws IOException {
        
        String ts = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss'\t'").format(new Date());
        String thisDay = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        File thisDayFile = new File(thisDay +"_log.txt");
        
        BufferedWriter bufw = new BufferedWriter(new FileWriter(thisDay +"_log.txt", true));
        bufw.flush();
        bufw.append("Loki on tallennettu " + ts + "\n");         
        for (int m = 0; m < listLog.getItems().size(); m++) {
            bufw.append(listLog.getItems().get(m)+"\n");
        }
        bufw.close();
    }
    @FXML
    private void changePackageAction(ActionEvent event) {
        
        if (!storage.getStorage().isEmpty()) {
            changeListView.getItems().clear();
            changeListView.setVisible(true);
            infoLabel.setVisible(true);
            changeListView.getItems().addAll(storage.getStorage());
        }

        
        
    }

    @FXML
    private void confirmAction(ActionEvent event) {
        String frag;
        Package pack = changeListView.getSelectionModel().getSelectedItem();
        // chooses the highlighted package from the listview
        
        pack.setItem(itemBox.getValue());
        pack.setPackageClass((int)packageClass.getSelectedToggle().getUserData()); 
       pack.setDepartureAddress(departureSmartPost.getValue().getAddress());
        pack.setDestinationAddress(destinationSmartPost.getValue().getAddress());
        //changeListView.getItems().clear();
        //changeListView.getItems().addAll(storage.getStorage());
        errorField.setText("Muutokset vahvistettu");
        changeListView.setVisible(false);
        confirmButton.setVisible(false);
        
        if (pack.isFragile()) {
                frag = "Särkyvä esine";  
            } else {
                frag = "Ei särkyvä esine";
            }
        
        String logOutput = "Muokattiin paketin tietoja: \n" +
                "Sisältö: " + itemBox.getValue().getItemName() + "\n" +
                "Mistä: " + departureSmartPost.getValue().getAddress() + ", " + pack.getDepCity() + "\n" +
                "Minne: " + destinationSmartPost.getValue().getAddress() + ", " + pack.getDestCity() +  "\n" +
                "Pakettiluokka: " + (int)packageClass.getSelectedToggle().getUserData() +"\n" +
                frag;
        
        listLog.getItems().add(logOutput);
        packageClass.getSelectedToggle().selectedProperty().setValue(false);
        departureBox.setValue(null);
        destinationBox.setValue(null);
        departureSmartPost.setValue(null);
        destinationSmartPost.setValue(null);
        infoLabel.setVisible(false);

        }

    @FXML
    private void classInfoAction(ActionEvent event) {
        
        
        listLog.getItems().add("Pyydettiin tietoja pakettiluokista");
        
        try {
            // Opens a new window for info about the package classes
            Stage infoView = new Stage();
            Parent window = FXMLLoader.load(getClass().getResource("infoFXML.fxml"));
            Scene scene = new Scene(window);
            infoView.setScene(scene);
            infoView.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
    }


    private void displayImage(ActionEvent event) {
        pictureLabel.setVisible(true);
        if (itemBox.getValue() == null) {
            pictureLabel.setText("Et ole valinnut esinettä");
            //
        } else if (itemBox.getValue().getItemName().equals("Pehmeä nallekarhu")) {
            Image image = new Image(getClass().getResourceAsStream("/Pictures/teddy.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } else if (itemBox.getValue().getItemName().equals("Langaton Bluetooth-kaiutin")) {
            Image image2 = new Image(getClass().getResourceAsStream("/Pictures/btspeaker.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image2);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } else if (itemBox.getValue().getItemName().equals("Paksu parkatakki")) {
            Image image3 = new Image(getClass().getResourceAsStream("/Pictures/winterparka.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image3);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } else if (itemBox.getValue().getItemName().equals("Pieni peili")) {
            Image image4 = new Image(getClass().getResourceAsStream("/Pictures/smirror.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image4);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } else if (itemBox.getValue().getItemName().equals("Smart-TV")) {
            Image image5 = new Image(getClass().getResourceAsStream("/Pictures/smart.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image5);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        } else if (itemBox.getValue().getItemName().equals("Mikroaaltouuni")) {
            Image image6 = new Image(getClass().getResourceAsStream("/Pictures/micro.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image6);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);

        }  else if (itemBox.getValue().getItemName().equals("Teekkarilakki")) {
            Image image7 = new Image(getClass().getResourceAsStream("/Pictures/teekkaricap.png"));
            pictureLabel.setText("Kuva esineestä:");
            imageView.setImage(image7);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
        
        } else {
            pictureLabel.setText("Tällä esineellä ei ole kuvaa!");
        }
    }


    @FXML
    private void downloadAction(ActionEvent event) throws FileNotFoundException, IOException {
        // items' names, weights, sizes and fragile info needs to be in the correct order in the text file
        // otherwise adding items won't work
        String inputFileName;
        String iName;
        String iWeight;
        String iWidth;
        String iDepth;
        String iHeight;
        String fragile;
        

        if (listInputField.getText().equals("")) {
            inputFileName = "itemlibrary.txt";
        } else {
            inputFileName = listInputField.getText();
        }

        Scanner sc = new Scanner(new File(inputFileName));
        while ((sc.hasNextLine())) {
            iName = sc.nextLine();
            iWeight = sc.nextLine();
            iWidth = sc.nextLine();
            iDepth = sc.nextLine();
            iHeight = sc.nextLine();
            fragile = sc.nextLine();
            
            String[] splitName = iName.split(":");
            String[] splitWeight = iWeight.split(":");
            String[] splitWidth = iWidth.split(":");
            String[] splitDepth = iDepth.split(":");
            String[] splitHeight = iHeight.split(":");
            String[] splitFrag = fragile.split(":");
            
            iName = splitName[1];
            iWeight = splitWeight[1];
            iWidth = splitWidth[1];
            iDepth = splitDepth[1];
            iHeight = splitHeight[1];
            fragile = splitFrag[1];
            
            System.out.println(iName + iWeight + iDepth + iHeight + fragile);
            
            items.addItem(new Item(iName, Double.parseDouble(iWeight), Integer.parseInt(iWidth), Integer.parseInt(iDepth), Integer.parseInt(iHeight), Boolean.parseBoolean(fragile)));
        }

        itemBox.getItems().clear(); // refresh the item list
        itemBox.getItems().addAll(items.getItemList());
        
        String logOutput;
        logOutput = "Ladattiin käyttäjän omat esineet tiedostosta: " + inputFileName;
        listLog.getItems().add(logOutput);

     
    }

    @FXML
    private void hideOtherClasses(ActionEvent event) {
        if (fastShipmentCheck.isSelected() == true) {
            firstClassButton.setSelected(true);
            secondClassButton.setVisible(false);
            thirdClassButton.setVisible(false);
        } else {
            firstClassButton.setSelected(false);
            secondClassButton.setVisible(true);
            thirdClassButton.setVisible(true);
        }
        
    }


    @FXML
    private void getPackageInfo(MouseEvent event) {
        
        if (changeListView.getSelectionModel().isEmpty()) {
            //
        } else {
            confirmButton.setVisible(true);
            Package pack = changeListView.getSelectionModel().getSelectedItem();

            changeListView.getSelectionModel().isEmpty();

            if (pack == null) {
                // nothing
            } else {
                itemBox.setValue(pack.getItem());
                int tempClass = pack.getPackageClass();
                if (tempClass == 1) {
                    firstClassButton.setSelected(true);
                } else if (tempClass == 2) {
                    secondClassButton.setSelected(true);
                } else {
                    thirdClassButton.setSelected(true);
                }

                departureBox.setValue(pack.getDepCity());
                destinationBox.setValue(pack.getDestCity());
                departureSmartPost.setValue(pack.getDepSp());
                destinationSmartPost.setValue(pack.getDestSp());
            }  
        }
    }


    @FXML
    private void setDepatureCity(ActionEvent event) {
        
        if (departureSmartPost.getValue() != null) {
            departureBox.setValue(departureSmartPost.getValue().getCity());
        }
    }

    @FXML
    private void setDestinationCity(ActionEvent event) {
        
        if (destinationSmartPost.getValue() != null) {
            destinationBox.setValue(destinationSmartPost.getValue().getCity());
        } 
    }

    @FXML
    private void setItemInfo(ActionEvent event) {
        imageView.setVisible(true);
        if (itemBox.getValue() != null) {
            selectedName.setText("Valitun esineen nimi: "+itemBox.getValue().getItemName());
            selectedSize.setText("Valitun esineen koko: " + Integer.toString((int)itemBox.getValue().getSizeList().get(0)) +"cm *" +
                    Integer.toString((int)itemBox.getValue().getSizeList().get(1)) + "cm *" +
                    Integer.toString((int) itemBox.getValue().getSizeList().get(2))+ "cm");
            selectedWeight.setText("Valitun esineen paino: " + Double.toString(itemBox.getValue().getWeight()) + " kilogrammaa");
         
        
            if (itemBox.getValue().getItemName().equals("Pehmeä nallekarhu")) {
                Image image = new Image(getClass().getResourceAsStream("/Pictures/teddy.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);
            } else if (itemBox.getValue().getItemName().equals("Langaton Bluetooth-kaiutin")) {
                Image image2 = new Image(getClass().getResourceAsStream("/Pictures/btspeaker.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image2);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);
            } else if (itemBox.getValue().getItemName().equals("Paksu parkatakki")) {
                Image image3 = new Image(getClass().getResourceAsStream("/Pictures/winterparka.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image3);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);
            } else if (itemBox.getValue().getItemName().equals("Pieni peili")) {
                Image image4 = new Image(getClass().getResourceAsStream("/Pictures/smirror.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image4);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);
            } else if (itemBox.getValue().getItemName().equals("Smart-TV")) {
                Image image5 = new Image(getClass().getResourceAsStream("/Pictures/smart.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image5);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);
            } else if (itemBox.getValue().getItemName().equals("Mikroaaltouuni")) {
                Image image6 = new Image(getClass().getResourceAsStream("/Pictures/micro.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image6);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);

            }  else if (itemBox.getValue().getItemName().equals("Teekkarilakki")) {
                Image image7 = new Image(getClass().getResourceAsStream("/Pictures/teekkaricap.png"));
                pictureLabel.setText("Kuva esineestä:");
                imageView.setImage(image7);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setCache(true);
        
        } else {
            pictureLabel.setText("Tällä esineellä ei ole kuvaa!");
        }
    }
} 
    

    @FXML
    private void addFeather(ActionEvent event) {
        
        addedWeight += 10; // add more weight
        
    }

    @FXML
    private void showHistory(ActionEvent event) throws FileNotFoundException, IOException {
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate chosenDate = calendar.getValue();
        newDate = formatter.format(chosenDate);
        //String thisDay = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        File dayFile = new File(newDate +"_log.txt");
        String line;

        historyArea.clear();
        
        if (dayFile.exists()) {
            BufferedReader br = new BufferedReader(new FileReader(newDate +"_log.txt"));
            
            while((line = br.readLine()) != null) {
                
                historyArea.appendText(line+"\n");
                
            }
        } else {
            historyArea.setText("Kyseille päivämäärälle ei löytynyt lokitietoja.");
        }
             
    }
    @FXML
    private void loadSerStorage(ActionEvent event) throws IOException, ClassNotFoundException, FileNotFoundException {
        
        if (newDate == null) {
            historyView.getItems().clear();
            historyView.getItems().add("Et ole valinnut päivämäärää!");
        } else {

        File f = new File(newDate +"_storage.ser");
        if (f.exists()) {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(newDate +"_storage.ser"));
        
            ArrayList historyStorage = (ArrayList) in.readObject();      
            historyView.getItems().clear();
            listLog.getItems().add("Ladattiin varastotilanne " + newDate);
            historyView.getItems().add("TIMO-järjestelmän varastotilanne " + newDate);

            packageBox.getItems().clear(); // clears the combobox
            packageBox.getItems().addAll(historyStorage);   
            for (int j = 0; j < historyStorage.size(); j++) {
                historyView.getItems().add(historyStorage.get(j).toString());
              
            } 
        } else {
            historyView.getItems().clear();
            historyView.getItems().add("Päivämäärälle " + newDate + " ei löytynyt varastotilannetta");
        }
        
        }
        
         
    }

    @FXML
    private void saveSerStorage(ActionEvent event) throws IOException, ClassNotFoundException {
        
        ArrayList tempStorage = storage.getStorage();
        
        String thisDay = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        
        File tempFile = new File(thisDay +"_storage.ser");
        
        if (tempFile.exists()) {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(thisDay +"_storage.ser"));     
            ArrayList appendStorage = (ArrayList) in.readObject();
            
            tempStorage.addAll(appendStorage);
            
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(thisDay +"_storage.ser"));
            out.writeObject(tempStorage); // append the ser-storage
            out.close();
            listLog.getItems().add("Tallennettiin varastotilanne.");
        } else {
            
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(thisDay +"_storage.ser"));
            out.writeObject(storage.getStorage()); // write the current storage to the ser-file
            out.close();
        } 
    
    }

} // end of file
        

