// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Package-class from which new package classes are inherited
// Contains info for the route and item inside the specific package


import java.util.ArrayList;


public class Package implements java.io.Serializable {
    
    private int packageClass;
    private Item item;
    private ArrayList<Double> geoArray; // Arraylist for the latitude and longitude
    private boolean fragile;
    private String departureAddress;
    private String destinationAddress;
    private String depAvailability;
    private String depPostOffice;
    private String depCity;
    private String depPostcode;
    
    private String destAvailability;
    private String destPostOffice;
    private String destCity;
    private String destPostcode;
    
    private Smartpost depSp;
    private Smartpost destSp;
    
    private double packageWeight;
    
    public Package(int packageClass, Item item, ArrayList<Double> geopoint, ArrayList<Double> geopoint2, boolean f, String a, String b,
            String depAvail, String depOffice, String depC, String depCode, String destAvail, String destOffice, String destC, String destCode,
            Smartpost deSp, Smartpost detSp, double w) { // builder
        
        geopoint.addAll(geopoint2);

        this.packageClass = packageClass;
        this.item = item;
        this.geoArray = geopoint;
        fragile = f;
        departureAddress = a;
        destinationAddress = b;
        depAvailability = depAvail;
        depPostOffice = depOffice;
        depCity = depC;
        depPostcode = depCode;
        
        destAvailability = destAvail;
        destPostOffice = destOffice;
        destCity = destC;
        destPostcode = destCode;
        
        depSp = deSp;
        destSp = detSp;
        packageWeight = w;
    }

    public int getPackageClass() {
        return packageClass;
    }

    public Item getItem() {
        return item;
    }

    public ArrayList<Double> getGeoArray() {
        return geoArray;
    }

    public boolean isFragile() {
        return fragile;
    }

    public String getDepartureAddress() {
        return departureAddress;
    }

    public String getDepAvailability() {
        return depAvailability;
    }

    public String getDepPostOffice() {
        return depPostOffice;
    }

    public String getDepCity() {
        return depCity;
    }

    public String getDepPostcode() {
        return depPostcode;
    }

    public String getDestAvailability() {
        return destAvailability;
    }

    public String getDestPostOffice() {
        return destPostOffice;
    }

    public String getDestCity() {
        return destCity;
    }

    public String getDestPostcode() {
        return destPostcode;
    }

    public Smartpost getDepSp() {
        return depSp;
    }

    public Smartpost getDestSp() {
        return destSp;
    }
    
    

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public void setDepAvailability(String depAvailability) {
        this.depAvailability = depAvailability;
    }

    public void setDepPostOffice(String depPostOffice) {
        this.depPostOffice = depPostOffice;
    }

    public void setDepCity(String depCity) {
        this.depCity = depCity;
    }

    public void setDepPostcode(String depPostcode) {
        this.depPostcode = depPostcode;
    }

    public void setDestAvailability(String destAvailability) {
        this.destAvailability = destAvailability;
    }

    public void setDestPostOffice(String destPostOffice) {
        this.destPostOffice = destPostOffice;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public void setDestPostcode(String destPostcode) {
        this.destPostcode = destPostcode;
    }

    public void setDepSp(Smartpost depSp) {
        this.depSp = depSp;
    }

// SETTERS
    public void setDestSp(Smartpost destSp) {
        this.destSp = destSp;
    }

    public void setDepartureAddress(String departureAddress) {
        this.departureAddress = departureAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public void setGeoArray(ArrayList<Double> geoArray) {
        this.geoArray = geoArray;
    }

    public void setPackageClass(int packageClass) {
        this.packageClass = packageClass;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setPackageWeight(double packageWeight) {
        this.packageWeight = packageWeight;
    }
    
    
    
    @Override
    public String toString() {
        return ("Paketti: " + this.getItem().getItemName() +"\n" +
                "Pakettiluokka: " + this.getPackageClass() + "\n" +
                "Mistä: " + this.getDepartureAddress() + ", " + this.getDepCity() +"\n" +
                "Minne: " + this.getDestinationAddress() + ", " + this.getDestCity() +  "\n");
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

//    public void setPackageClass(int t) {
//        t = packageClass;
//    }
 
} // EOF
