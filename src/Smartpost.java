
import java.util.ArrayList;

// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Smartpost-class contains smartpost specific data
// Also has a subclass which tells the location of this specific Smartpost


public class Smartpost implements java.io.Serializable {
    
    private String city;
    private String code;
    private GeoPoint geopoint;
    private double lat;
    private double lng;
    private String address;
    private String availability;
    private String postOffice;
    
    
    public Smartpost (String city, String code, Double lat, Double lng, String address, String availability, String postOffice) { // builder for smartpost
        this.city = city;
        this.code = code;
        this.geopoint = new GeoPoint(lat,lng);
        this.address = address;
        this.availability = availability;
        this.postOffice = postOffice;
    }

    public String getCity() {
        return city;
    }

    public String getCode() {
        return code;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public GeoPoint getGeopoint() {
        return geopoint;
    }

    
    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostOffice() {
        return postOffice;
    }
    
    @Override
    public String toString() {
        return address;
    }
    
}

class GeoPoint implements java.io.Serializable {

    ArrayList<Double> geoArray = new ArrayList();
    
    public GeoPoint(Double lat, Double lon) {
        geoArray.add(lat);
        geoArray.add(lon);
        // add latitude and longitude to the geoarray
    }

    public ArrayList<Double> getGeoArray() {
        return geoArray;
    }
    
    

}