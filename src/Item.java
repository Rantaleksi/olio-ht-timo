
import java.util.ArrayList;

// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Item-class holds the information of the items which are created by the user


public class Item implements java.io.Serializable {

    private String itemName;
    private double weight;
    private boolean fragile;
    private ArrayList sizeList = new ArrayList();

    public Item (String a, double b, int x, int y, int z, boolean p) { // builder
        itemName = a;
        weight = b;
        sizeList.add(x); // width
        sizeList.add(y); // depth
        sizeList.add(z); // height
        fragile = p;
    }

    public String getItemName() {
        return itemName;
    }


    public double getWeight() {
        return weight;
    }
    
    @Override
    public String toString() {
        return itemName;
    }

    public boolean isFragile() {
        return fragile;
    }

    public ArrayList getSizeList() {
        return sizeList;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    


} // end of class

// SEVEN BASIC ITEMS


class TeddyBear extends Item {
    public TeddyBear() {
        super("Pehmeä nallekarhu", 0.4, 10, 30, 15, false);
    }
}

class BluetoothSpeaker extends Item {
    
    public BluetoothSpeaker() {
        super("Langaton Bluetooth-kaiutin", 1.8, 15,25,15, true);
    }
}

class WinterParka extends Item {
    
    public WinterParka() {
        super("Paksu parkatakki", 3.1, 50,15,40, false);
    }
}

class SmallMirror extends Item {

    public SmallMirror() {
        super("Pieni peili", 0.3, 20,1,30, true);
    }
} 

class SmartTV extends Item {

    public SmartTV() {
        super("Smart-TV", 22, 100,60,10, true);
    }
}

class MicrowaveOven extends Item {

    public MicrowaveOven() {
        super("Mikroaaltouuni", 15, 30,30,30, true);
    }
}

class TeekkariHat extends Item {

    public TeekkariHat() {
        super("Teekkarilakki", 1, 25,10,25, false);
    }
} 