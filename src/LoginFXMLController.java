/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Antti
 */
public class LoginFXMLController implements Initializable {

    @FXML
    private TextField unField;
    @FXML
    private PasswordField pwField;
    @FXML
    private Button loginButton;
    @FXML
    private ImageView loginLogo;
    @FXML
    private Label invalidField;

    Users users = new Users();


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image logo = new Image(getClass().getResourceAsStream("/Pictures/timoLogo.png"));
        loginLogo.setImage(logo);

    }    

    @FXML
    private void loginAction(ActionEvent event) throws IOException {
        
        int stop = 0;
        int i = 0;
        
        while (stop == 0) {
            
            if (unField.getText().equals(users.getUserList().get(i).getUserName()) && pwField.getText().equals(users.getUserList().get(i).getPassWord())) {
            // launch the main program
                stop = 1;

                ((Node) (event.getSource())).getScene().getWindow().hide();
                Parent parent = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(parent);
                stage.setScene(scene);
                stage.show();
            
            } else if (i < users.getUserList().size()-1) {
                i++;
                
            } else {
                stop = 1;
                invalidField.setText("Väärä käyttäjätunnus tai salasana");
        }  
    }
 
    } // end of method


} // EOF



    

