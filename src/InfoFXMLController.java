

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Antti
 */
public class InfoFXMLController implements Initializable {

    @FXML
    private TextArea infoViewArea;
    @FXML
    private Button closeWindowButton;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // set info about the different package classes to the textfield
        infoViewArea.setText("1. luokan paketti\n" +
                    "Koko korkeintaan: 30*60*30cm\n" +
                    "Paino enintään: 5 kg\n" +
                    "HUOM! 1. luokan paketin voi lähettää enintään 150 km päähän\n"
                + "Särkyvät esineet tulevat todennäköisesti hajoamaan kuljetuksessa\n");
        
        infoViewArea.appendText("\n2. luokan paketti\n" +
                    "Koko korkeintaan: 20*60*20*cm\n" +
                    "Paino enintään: 3 kg\n" +
                    "2. luokan paketin tulee olla pienempi kuin muiden pakettien\n");
       
        infoViewArea.appendText("\n3. luokan paketti\n" +
                    "Koko korkeintaan: 100*60*50*cm\n" +
                    "Paino enintään: 85 kg\n"
                    + "Paino vähintään: 15 kg");
    }    

    @FXML
    private void cwAction(ActionEvent event) {
        
        ((Node) (event.getSource())).getScene().getWindow().hide();
        // close the window
    }
    
}

