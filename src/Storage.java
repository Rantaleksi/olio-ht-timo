// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Storage-class is used for storing previously created packages


import java.util.ArrayList;
import java.util.logging.Logger;



public class Storage implements java.io.Serializable {
    
    private static Storage instance = null;
    private ArrayList<Package> packageList;
    private int packageCount;

    protected Storage() {
        packageList = new ArrayList<Package>();
        packageCount = 0;
    }
    
    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }
    
    public void addPackage(Package p) {
        packageList.add(p);
        packageCount++;
    }

    
    
    public ArrayList<Package> getStorage() {
        return packageList;
    }
    
    public void removePackage (Package p) {
        packageList.remove(p);
        packageCount--;
    }

    public int getPackageCount() {
        return packageCount;
    }

    public ArrayList<Package> getPackageList() {
        return packageList;
    }
    
    
    
    
}
