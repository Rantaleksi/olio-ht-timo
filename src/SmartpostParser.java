// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// MClass for parsing the XML data which is loaded from open data
// Creates new Smartpost-objects

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class SmartpostParser {

    private String content = "";
    private Document doc;
   
    private ArrayList<Smartpost> smartposts = new ArrayList<>();
    
    // cities list inside smartpostlist
    private ArrayList<String> cities = new ArrayList<>(); 
    
//  builder for the parser - also calls loads-method
    public SmartpostParser(){
        try {
            load();
        } catch (IOException ex) {
            Logger.getLogger(Smartposts.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Smartpostlistan lataamisessa on tapahtunut virhe.");
        }
    }
 
//  Loads required content, builds document and parses data.
    private void load() throws IOException{
        try{
        
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
        
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
            String line;
            while((line = br.readLine()) != null){
                content += line + "\n";
            }
            
//          build the document
            buildDoc();
//          parse the data
            parseCurrentData();
        }
        catch(MalformedURLException ex){
            System.err.println("Huono urli");
        }
    }
    
    //Builds document
    private void buildDoc(){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(Smartposts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseCurrentData(){
        
        NodeList nodes = doc.getElementsByTagName("place");
        
        for(int i = 0; i < nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            String city = getValue("city", e);
            String address = getValue("address", e);
            String code = getValue("code", e);
            double lat = Double.parseDouble(getValue("lat", e));
            double lng = Double.parseDouble(getValue("lng", e));
            String availability = getValue("availability", e);
            String postoffice = getValue("postoffice", e);
            
//          creates a new object to the list
            smartposts.add(new Smartpost(city, code, lat, lng, address, availability, postoffice));
            
            
//          adds the cities to the citylist
            if(cities.contains(city)==false){
                cities.add(city);
            }
        }
    }
    
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    public ArrayList<Smartpost> getSmartposts() {
        return smartposts;
    }
    
//  Tähän lisäsin getCitiesin, koska lisäsin cities-listan tähän parseriin    
    public ArrayList<String> getCities(){
        return cities;
    }
    
} // EOF
