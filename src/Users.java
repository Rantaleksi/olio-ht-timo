
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Antti
 */
public class Users {
    private ArrayList<User> userList = new ArrayList<>();
    
    public Users() {
               
        try {
            BufferedReader br = new BufferedReader(new FileReader("userlist.txt"));
            String inputLine;

            while((inputLine = br.readLine()) != null) {
                String u = inputLine;
                String p = br.readLine();

                userList.add(new User(u, p));
                
//            }
            
            }
        } catch (IOException ex) {
            System.out.println("En päässyt");
            Logger.getLogger(LoginFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<User> getUserList() {
        return userList;
    }
        
    public void addUser(User u) {
        userList.add(u); 
    }
    
    
}


class User {
    private String userName;
    private String passWord;
    
    public User(String un, String pw) {
        userName = un;
        passWord = pw;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassWord() {
        return passWord;
    }
    
}